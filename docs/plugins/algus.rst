.. _algus.plugins.algus:

======================================
``algus`` (main plugin for Lino Algus)
======================================

In Lino Algus this plugin defines the :xfile:`locale` directory for all
translations.

.. contents::
  :local:

.. include:: /../docs/shared/include/tested.rst

>>> import lino
>>> lino.startup('lino_algus.projects.algus1.settings')
>>> from lino.api.doctest import *
