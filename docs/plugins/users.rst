.. doctest docs/plugins/users.rst
.. _algus.plugins.users:

==================================
``users`` in Lino Algus
==================================

The :mod:`lino_algus.lib.users` plugin extends :mod:`lino.modlib.users`.

.. contents::
  :local:

.. include:: /../docs/shared/include/tested.rst

>>> import lino
>>> lino.startup('lino_algus.projects.algus1.settings')
>>> from lino.api.doctest import *


Available user types
====================

Lino Algus knows the following :term:`user types <user type>`:

>>> rt.show(rt.models.users.UserTypes)
======= =========== ===============
 value   name        text
------- ----------- ---------------
 000     anonymous   Anonymous
 100     user        User
 500     staff       Staff
 900     admin       Administrator
======= =========== ===============
<BLANKLINE>

A :term:`demo site` has the following users:

>>> rt.show(rt.models.users.UsersOverview)
... #doctest: +NORMALIZE_WHITESPACE +REPORT_UDIFF
========== ===================== ==========
 Username   User type             Language
---------- --------------------- ----------
 robin      900 (Administrator)   en
 rolf       900 (Administrator)   de
 romain     900 (Administrator)   fr
========== ===================== ==========
<BLANKLINE>


The site manager
================

Robin is a :term:`site manager`, he has a complete menu.

>>> show_menu('robin')
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
- Configure :
  - System : Users, Site configuration
- Explorer :
  - System : Authorities, User types, User roles
- Site : About, User sessions
