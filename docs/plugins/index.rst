.. _algus.plugins:

================================
Plugins reference for Lino Algus
================================


.. toctree::
   :maxdepth: 1
   :glob:

   algus
   users
