.. doctest docs/topics/db.rst
.. _algus.topics.db:

================================
Database structure of Lino Algus
================================

This document describes the database structure.

.. contents::
  :local:


.. include:: /../docs/shared/include/tested.rst

>>> import lino
>>> lino.startup('lino_algus.projects.algus1.settings')
>>> from lino.api.doctest import *


>>> from lino.utils.diag import analyzer
>>> analyzer.show_db_overview()
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
10 plugins: lino, about, jinja, react, printing, system, users, algus, staticfiles, sessions.
4 models:
=================== ==================== ========= =======
 Name                Default table        #fields   #rows
------------------- -------------------- --------- -------
 sessions.Session    users.Sessions       3         ...
 system.SiteConfig   system.SiteConfigs   3         1
 users.Authority     users.Authorities    3         0
 users.User          users.AllUsers       20        3
=================== ==================== ========= =======
<BLANKLINE>
