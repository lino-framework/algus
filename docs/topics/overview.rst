.. doctest docs/topics/overview.rst
.. _algus.topics.overview:

========
Overview
========

This document should give an overview over Lino Algus.

.. contents::
  :local:

.. include:: /../docs/shared/include/tested.rst

>>> import lino
>>> lino.startup('lino_algus.projects.algus1.settings')
>>> from lino.api.doctest import *


>>> print(analyzer.show_complexity_factors())
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
- 10 plugins
- 4 models
- 4 user types
- 22 views
- 6 dialog actions
<BLANKLINE>
