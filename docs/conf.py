# -*- coding: utf-8 -*-
# fmt: off

import datetime

from atelier.sphinxconf import configure; configure(globals())
from lino.sphinxcontrib import configure; configure(
    globals(), 'lino_algus.projects.algus1.settings')

extensions += ['lino.sphinxcontrib.help_texts_extractor']
help_texts_builder_targets = {'lino_algus.': 'lino_algus.lib.algus'}

project = "Lino Algus"
copyright = '2016-{} Rumma & Ko Ltd'.format(datetime.date.today().year)

html_title = "Lino Algus"
# html_context.update(public_url='https://algus.lino-framework.org')
