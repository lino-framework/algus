.. _algus:

===========
Lino Algus
===========

Welcome to the **Lino Algus** project homepage.

.. note:: The following content, if you happen to see it, is not
          meaningful. Remove this note from your copy of
          :file:`docs/index.rst`.


Content
========

.. toctree::
   :maxdepth: 1

   get/index
   guide/index
   topics/index
   plugins/index
   api/index
   changes
