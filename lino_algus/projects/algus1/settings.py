# -*- coding: UTF-8 -*-
# Copyright 2016-2023 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino_algus.lib.algus.settings import *


class Site(Site):

    is_demo_site = True
    the_demo_date = 20150523
    languages = "en de fr"
    default_ui = 'lino_react.react'


SITE = Site(globals())

DEBUG = True
USE_TZ = True
TIME_ZONE = 'UTC'

# the following line should not be active in a checked-in version
# DATABASES['default']['NAME'] = ':memory:'
