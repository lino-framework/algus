# -*- coding: UTF-8 -*-
# Copyright 2016-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""This is the main module of Lino Algus.

.. autosummary::
   :toctree:

   lib
   projects


"""

__version__ = '24.10.0'


intersphinx_urls = dict(docs="https://lino-framework.gitlab.io/algus")
srcref_url = 'https://gitlab.com/lino-framework/algus/blob/master/%s'
doc_trees = ['docs']
