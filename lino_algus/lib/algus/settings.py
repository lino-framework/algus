# -*- coding: UTF-8 -*-
# Copyright 2016-2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.projects.std.settings import *
import lino_algus

class Site(Site):

    verbose_name = "Lino Algus"
    version = lino_algus.__version__

    demo_fixtures = ['std', 'demo', 'demo2']
    user_types_module = 'lino_algus.lib.algus.user_types'
    migration_class = 'lino_algus.lib.algus.migrate.Migrator'

    def get_installed_plugins(self):
        """Implements :meth:`lino.core.site.Site.get_installed_plugins`.

        """
        yield super().get_installed_plugins()
        yield 'lino_algus.lib.users'
        yield 'lino_algus.lib.algus'

    def setup_quicklinks(self, ut, tb):
        super().setup_quicklinks(ut, tb)
