=================================================
Lino Algus - A template for new Lino applications
=================================================

A repository that you can use as template for writing your own `Lino
<https://www.lino-framework.org/>`_ application.

Basic use is as follows:

- Find a short one-word name for your application, for example "Lino
  Example".

- Run the following::

      $ pip install getlino
      $ getlino startproject example

  See :cmd:`getlino startproject` for more information.

- Install your application into the Python environment (using develop mode)::

    cd ~/lino/lino_local/example
    pip install -e .

- To start the demo project, run the following commands::

    $ cd lino_example/projects/example1
    $ python manage.py prep
    $ python manage.py runserver

- To publish your your project: Create a GitLab account if you haven't
  already, log in, click "New project", select "Create blank project", give a
  project name (e.g. "foo"), submit. Then follow the "Command line instructions"
  to "Push an existing folder" given by GL in your new project page::

    cd ~/lino/lino_local/example
    git init
    git remote add origin git@gitlab.com:username/example.git
    git add .
    git commit -m "Initial commit"
    git push -u origin main

Note: "algus" is the Estonian word for "start". We did not name this template
"Lino Start" because the word "start" is more likely to occur in variable names
or text which is not related to the project name.
